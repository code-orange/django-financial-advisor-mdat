from datetime import datetime, timedelta

from django.db import models


class MdatStock(models.Model):
    symbol = models.CharField(max_length=200, unique=True)
    tstamp = models.DateTimeField(default=datetime.now)

    def get_mean_for_seconds(self, seconds=3600):
        return {
            "bid_price": self.mdatstocklog_set.filter(
                tstamp__gt=datetime.now() - timedelta(seconds=seconds)
            ).aggregate(models.Avg("bid_price")),
            "ask_price": self.mdatstocklog_set.filter(
                tstamp__gt=datetime.now() - timedelta(seconds=seconds)
            ).aggregate(models.Avg("ask_price")),
        }

    class Meta:
        db_table = "mdat_stock"


class MdatStockLog(models.Model):
    stock = models.ForeignKey(MdatStock, models.DO_NOTHING)
    tstamp = models.DateTimeField(default=datetime.now)
    bid_price = models.DecimalField(max_digits=10, decimal_places=2)
    ask_price = models.DecimalField(max_digits=10, decimal_places=2)

    class Meta:
        db_table = "mdat_stock_log"
