from pprint import pprint

from django.conf import settings
from django.core.management.base import BaseCommand

from django_financial_advisor_mdat.django_financial_advisor_mdat.models import MdatStock


class Command(BaseCommand):
    help = "Get Stock mean"

    def handle(self, *args, **options):
        stock_data = MdatStock.objects.get(symbol=settings.PROJ_GENERAL_ISIN)
        pprint(stock_data.get_mean_for_seconds(3600))
